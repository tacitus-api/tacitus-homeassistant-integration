"""Definitions of all HDD sensors supoorted by tacitus integration."""

from homeassistant.components.binary_sensor import (
    BinarySensorDeviceClass,
    BinarySensorEntity,
)
from homeassistant.components.sensor import (
    SensorDeviceClass,
    SensorEntity,
    SensorStateClass,
)
from homeassistant.const import TEMP_CELSIUS
from homeassistant.core import callback
from homeassistant.helpers.device_registry import DeviceInfo
from homeassistant.helpers.update_coordinator import (
    CoordinatorEntity,
    DataUpdateCoordinator,
)

from ..const import DOMAIN


class SystemSensorBase(CoordinatorEntity):
    """Shared basic structure for all HDD sensor entities."""

    _name_template: str = "{} sensor"
    _attr_name: str | None
    _sensor_posfix = ""
    _attr_device_info: DeviceInfo | None = None
    _attr_unique_id: str | None = None

    def __init__(
        self, coordinator: DataUpdateCoordinator
    ) -> None:
        """Entity is identified by HDDs serial number, path can change."""
        super().__init__(coordinator)

        # TODO: Include server id in unique id somehow
        self._attr_unique_id = (
            f"{DOMAIN}_system_{self._sensor_posfix}"
        )

        self._attr_device_info = DeviceInfo(
            identifiers={(DOMAIN, "system")},
            name=f"system",
            manufacturer="Scrin homelab",
            model="Carbon",
            sw_version="0.0.1",
        )


class SystemSensor(SystemSensorBase, SensorEntity):
    """SensorEntity extended of HDD basic functions."""

    pass


class CpuLoadM1Sensor(SystemSensor):
    """Representation of a Sensor."""

    _attr_name = "CPU load m1"
    _name_template = "CPU {} load m1"
    #_attr_native_unit_of_measurement = TEMP_CELSIUS
    #_attr_device_class = SensorDeviceClass.TEMPERATURE
    _attr_state_class = SensorStateClass.TOTAL
    _sensor_posfix = "cpu_load_m1"

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self._attr_native_value = self.coordinator.data.get("result").get("cpu").get("load1")
        self.async_write_ha_state()

class CpuLoadM5Sensor(SystemSensor):
    """Representation of a Sensor."""

    _attr_name = "CPU load m5"
    _name_template = "CPU {} load m5"
    #_attr_native_unit_of_measurement = TEMP_CELSIUS
    #_attr_device_class = SensorDeviceClass.TEMPERATURE
    _attr_state_class = SensorStateClass.TOTAL
    _sensor_posfix = "cpu_load_m5"

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self._attr_native_value = self.coordinator.data.get("result").get("cpu").get("load5")
        self.async_write_ha_state()

class CpuLoadM15Sensor(SystemSensor):
    """Representation of a Sensor."""

    _attr_name = "CPU load m15"
    _name_template = "CPU {} load m15"
    #_attr_native_unit_of_measurement = TEMP_CELSIUS
    #_attr_device_class = SensorDeviceClass.TEMPERATURE
    _attr_state_class = SensorStateClass.TOTAL
    _sensor_posfix = "cpu_load_m15"

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        self._attr_native_value = self.coordinator.data.get("result").get("cpu").get("load15")
        self.async_write_ha_state()

class RamUsedSensor(SystemSensor):
    """Representation of a Sensor."""

    _attr_name = "RAM used"
    _name_template = "RAM {} used"
    #_attr_native_unit_of_measurement = TEMP_CELSIUS
    #_attr_device_class = SensorDeviceClass.TEMPERATURE
    _attr_state_class = SensorStateClass.TOTAL
    _sensor_posfix = "ram_used"

    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""
        memory = self.coordinator.data.get("result").get("memory")
        self._attr_native_value = memory.get("used").get("value")
        self._attr_native_unit_of_measurement = memory.get("used", {}).get("unit")
        self.async_write_ha_state()
